\documentclass[12pt]{article}
\usepackage{sbc-template}
\usepackage{url}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{comment}
\usepackage{scalefnt}
%\usepackage[brazilian]{babel}

\sloppy

\title{Mezuro Platform: Source Code Tracking Network}

\author{Paulo Meirelles, Carlos Morais, Rafael M. Martins,
\\Fabio Kon, Carlos Santos Jr., José Carlos Maldonado}
\address{
FLOSS Competence Center - University of S\~ao Paulo
\email{\{paulormm,morais,fabio.kon,denner\}@ime.usp.br}
\email{\{rmartins,jcmaldon\}@icmc.usp.br}
}

\usepackage{xcolor}
\newcommand{\TODO}[1]{
  {
    \color{red}\textbf{{#1} (TODO)}
  }
}

\begin{document}

\maketitle
%-------------------------------------------------------------------------------
\begin{abstract}
Free software communities tend to work based on 
technical approaches. Specially, they look at the source code.
%
However, despite the ``show me the code'' culture, source code metrics are
often not perceived as an indicator of quality.
%
To promote the use of source code metrics to optimize free software development,
we are defining a systematic approach and developing tools to use,
interpret, and understand software metrics.

\end{abstract}
%-------------------------------------------------------------------------------

\section{Introduction}
\label{introduction}

There is a significant gap between the numbers of lines of code which a
software engineer reads and writes~\cite{martin2008}.
%
Usually, software engineers read hundreds of lines of code to understand
a software implementation before making improvements.
%
In short, source code should be written to be read by people. Software
engineers need to analyze source code frequently and the practice of Software 
Engineering as a whole requires a thorough comprehension of the project, of
which the source code is a very important part.

In this context, software source code metrics can help software engineers to
observe and determine source code quality.
%
Also, they can support the development of clean --
i.e., clear, flexible, and simple -- code~\cite{martin2008}.
%
However, many free software projects do not practice source code quality
evaluation and the tools available for that are few and not always adequate.
%
This lack of systematic code evaluation leaves a lot of room for improvement
in the development of free software~\cite{Michlmayr2005}.

To address these issues, we are investigating an approach to help software 
engineers make decisions about their code, while they are programming, at the
method and class level.
%
To make the best decisions, we argue that they should monitor attributes from
their source code. These decisions will influence source code
quality~\cite{beck2007}.

Our proposed platform is based on automated extraction of source code metrics
and an objective way to interpret their values, so that software engineers can 
monitor specific characteristics of their code.
%
For that, we are developing a technical network, called Mezuro, and a web
service, named Kalibro Metrics.

The remainder of this paper is organized as follows:
Section \ref{sec:related-projects} shows related projects and tools, as well as
Mezuro requirements.
Section \ref{sec:features} describes Mezuro features and
Section \ref{sec:architecture} presents Mezuro architecture.
Finally, Section \ref{sec:final-remarks} presents some conclusions and next steps
for Mezuro development.


%-------------------------------------------------------------------------------
\section{Related Projects and Tools}
\label{sec:related-projects}

So far, in the context of this work, we have found the following related 
projects:\footnote{Related projects: flossmetrics.org,
ohloh.net,
qualoss.org,
sqo-oss.org,
qsos.org,
fossology.org,
qualipso.org,
code.google.com/p/hackystat}
%

\begin{itemize}

\item \textbf{FLOSSMetrics} (Free/Libre Open Source Software Metrics) is a
project that uses existing methodologies and tools to provide a large database
with information about free software development.

\item \textbf{Ohloh} is a website that provides a suite of web services and an
on-line community platform that aims at building an overview of free software
development.

\item \textbf{Qualoss} (Quality in Open Source Software) is a methodology to
automate the quality measurement of free software projects, using tools to
analyze the source code and the project repository information.

\item \textbf{SQO-OSS} (Software Quality Assessment of Open Source Software)
provide a suite of tools for analysis and benchmarking of free software
projects.

\item \textbf{QSOS} (Qualification and Selection of Open Source Software) is a
methodology based on 4 steps: used reference definition; software evaluation; 
qualification of specific users context; selection and comparison of
software.

\item \textbf{FOSSology} (Advancing open source analysis and development) is a
project that provides a free database with information about software
licenses.

\item \textbf{QualiPSo} (Trust and Quality in Open Source Systems) defined
procedures to boost the use of free software and adoption of its development
practices within software industry. A set of tools was integrated with
QualiPSo Quality Platform.

\item \textbf{HackyStat} is an environment for analysis, visualization and
interpretation of software development process and product data.

\end{itemize}

We have identified the lack of the following features in these tools:
%
(i) to automatically collect source code metrics values, considering different
programming languages; and
%
(ii) to interpret measurement results, associating them with source code
quality.
%
Also, during our research and development activities, we studied or used 8 other
free software analysis tools\footnote{Related tool: analizo.org,
cccc.sourceforge.net,
checkstyle.sourceforge.net,
tools.libresoft.es/cmetrics,
cscope.sourceforge.net,
ccsl.icmc.usp.br/pt-br/projects/jabuti,
qualipso.dscpi.uninsubria.it/macxim,
metrics.sourceforge.net}:
% 
Analizo,
CCCC,
Checkstyle,
CMetrics,
Cscope,
JaBUTi,
MacXim,
and Metrics (Eclipse plugin).

As a consequence of this experience, we are developing the Mezuro platform to 
provide features to configure the visualization of source code metric results 
in a friendly way.
%
The main purpose is to help software engineers to spot design flaws to refactor, 
to help project managers to control source code quality, and to help software 
researchers to compare specific source code characteristics across free 
software projects.

By comparing all these tools we defined the following requirements for
our platform, according to our theoretical and practical needs to better
explore source code metrics:
%
(i) support metrics \textbf{thresholds} to allow different interpretations 
about their values;
%
(ii) support the analysis of different programming languages
(\textbf{multi-language});
%
(iii) provide clear interfaces for adding new metrics and supporting
different programming languages (\textbf{extensibility});
%
(iv) should be \textbf{free software}, available without
restrictions to allow other researchers to fully replicate our studies and 
results;
%
and (v) be supported by active developers who know the tool's
architecture (\textbf{maintained}).

\section{Features}
\label{sec:features}

We are developing Mezuro network to analyze and
understand metrics, as well as, apply our research results about clean code 
concepts, software measurement, and free software attractiveness.
%
It is free software under
AGPL~\footnote{GNU Affero General Public License (AGPL) --
gnu.org/licenses/agpl.html} license.
%
Mezuro uses Kalibro Metrics that is a web service that can connect to different kinds 
of source code repositories, download software projects, and run many integrated
metric collector and calculator tools automatically. Kalibro is licensed
LGPL\footnote{GNU Lesser General Public License (LGPL) -- gnu.org/licenses/lgpl.html}.

Mezuro itself is a ``social-technical'' network to track source code metrics. 
The platform promotes an open and collaborative networking to analyze software 
projects, specially free software, up to hundreds of thousands, through the 
automated tracking of their source code repositories.
%
In other words, it is an environment to enhance Kalibro service features
by using the potential of a social network.
%
The idea is based on, for example, the fact that people improve their writing
skills when they read good books and papers. Similarly, software engineers can
increase the quality of their source codes by finding related projects, 
reviewing good and clean codes, and comparing their source code characteristics.
%
Thus, Mezuro features can provide a collaborative technology roadmap for an
automated source code analysis approach.

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figure/mezuro_project.eps}
    \caption{Metric results visualization from Mezuro Platform.}
    \label{fig:mezuro-result}
  \end{center}
\end{figure}

So far, the following features are implemented.
%
\textbf{(i)} Download source code from Subversion, Git, Mercurial, Baazar, and
CVS repositories.
%
\textbf{(ii)} Creation of configurations, i.e., pre-defined sets of related metrics to 
be in used in the evaluation of software projects.
%
\textbf{(iii)} Creation of ranges associated with a metric and a qualitative evaluation.
%
\textbf{(iv)} Creation of new metrics (via JavaScript) based on the ones provided by
the metric collector tool.
%
\textbf{(v)} Calculation of statistics results for higher granularity modules
(e.g. average LOC of classes inside a package).
%
\textbf{(vi)} Possibility of exporting results to files.
%
\textbf{(vii)} Calculation of a grade for the source code analysis projects, based on
given weights for metrics and ranges. This allows cross-project comparisons.
%
\textbf{(viii)} Possibility of making interpretation more user-friendly by associating
colors with ranges.

Figure~\ref{fig:mezuro-result} shows an example of project information and
metrics vizualization with Mezuro.
%
In short, the user just need to give a
repository URL to Mezuro start to track source code metrics (such as Lines of
Code, Cyclomatic Complexity, Lack of Cohesion, Coupling, etc) from a project.
%
The source code analysis report can be accessed in an asynchronous way, i.e. 
when the users wish or need, since the history of source code metric values and 
analysis are recorded.
%
In addition, free and public project analysis are available to all users, 
as well as, any user can suggest metric threshold configurations and share them
on the Mezuro network.
%
This provides a Bazaar-like interaction environment~\cite{raymond1999}
to evolve and define the best way to explore the source code metrics potential.
%
In other words, a semi-automated software project analysis approach via 
interpretation of metrics.


Mezuro is available for test at \url{http://mezuro.org}. We can try to create a
Mezuro project. We must create a user and as logged user, we should
%
\textbf{(i)} create a ``community'';
\textbf{(ii)} go to the control panel from the created community;
\textbf{(iii)} click on the ``content management'' option;
\textbf{(iv)} choose a type of content called ``Kalibro project'';
\textbf{(v)} fill the following fields about a software project: name, description,
repository type, repository URL, and choose metric configuration. After that,
Mezuro sends all information via a request for Kalibro service. Depending on
source code size, Kalibro may take several time processing the source code
and collect the source code metrics.

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figure/mezuro_configuration.eps}
    \caption{Configuring a metric in the Mezuro Platform.}
    \label{fig:mezuro-configuration}
  \end{center}
\end{figure}

In the context of users specialist in software metrics, we can try to configure
set of metrics to share in the Mezuro network, as shown in
Figure~\ref{fig:mezuro-configuration}. As logged user, we should
\textbf{(i)} go to the user control panel;
\textbf{(ii)} click on the ``content management'' option;
\textbf{(iii)} choose a type of content called ``Kalibro configuration'';
\textbf{(iv)} fill name and description;
\textbf{(v)} choose a base tool (Analizo or CheckStyle, for example);
\textbf{(vi)} select a metric;
\textbf{(vii)} define threshold to the selected metric.


%%----------------------------------------------------------------------------%%
\section{Architecture}
\label{sec:architecture}

Mezuro is an instance of the Noosfero social network platform\footnote{
noosfero.org} with the Mezuro plugin activated.
%
Noosfero is an free software (web platform) for social and solidarity economy
networks with blog, e-Porfolios, CMS, RSS, thematic discussion, chat,
events agenda, and collective intelligence for solidarity economy.
%
It is being developed in Ruby programming language using the framework Rails.
%
Ruby on Rails core is based on Model View Controller (MVC) architecture. Rails
Plugins (such as Mezuro plugin) is an extension of the core framework.
Thus, Mezuro architecture also is based on MVC, following Rails and Noosfero
architecture.

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{figure/InteractionsDiagram.eps}
    \caption{Interaction overview of Mezuro and Kalibro Metrics}
    \label{fig:mezuro-interaction}
  \end{center}
\end{figure}


Plugins allow that developers can solve several problems without interact with the
entire code base. Mezuro was the first Noosfero plugin.
%
Its structure is built over an event-based paradigm. Noosfero fires an event in
a determined point and all plugins interested in that event are able to act.
%
Therefore, we have defined Mezuro plugin actions over each event without need to
rewrite Noosfero. This events are called hotspots.
%
Mezuro plugin development steps have led the Noosfero plugin framework and
defined which hotspots were implemented.


Mezuro plugin connects to Kalibro Metrics Web Service, as shown in
Figure~\ref{fig:mezuro-interaction}, via SOAP requests, which provide to access
the Kalibro Service
end-points\footnote{Kalibro end-points and WSDL definitions: valinhos.ime.usp.br:50688/KalibroService/KalibroEndpoint}:
Kalibro, Project, ProjectResult, ModuleResult, BaseTool, Configuration, and MetricConfiguration.
%
From a conceptual point of view, each Kalibro end-point is a service and Mezuro
works as orchestrator of these seven services, since each one works independently
and has different set of functionalities. Mezuro calls each end-point (service)
according to actions (requests) to Kalibro. In short, Mezuro performance and
scalability depend on this ``orchestration''. 


%%----------------------------------------------------------------------------%%
\section{Final Remarks and Future Developments}
\label{sec:final-remarks}

In this paper we described the Mezuro network, an platform that promotes the 
use of source code metrics to optimize free software development.
%
It is an environment for source code tracking, analysis, and visualization,
connected to the Kalibro Web Service via the Mezuro plugin for Noosfero
social network platform. 
%
Mezuro provides an environment where software engineers not only watch their
projects' metrics, but can also customize the presentation by defining their 
own threshold configurations, according to the project's context and their 
experiences in software development.

%
From a practical point of view, we are developing Mezuro to apply our research
results and support a better use of source code metrics. 
%
At this moment, we are implementing the source code analysis history
with a graphical software visualization to complete the Mezuro source code
metrics interpretation approach.
%
Future Mezuro Platform features include the integration with other metric
collector tools, especially to provide Perl, Python, and Ruby source code analysis.

Another feature currently in development through the platform's extensible
development interface is the integration with the JaBUTi \cite{jabuti} tool 
for the extraction of white-box testing metrics from free Java projects. 
Testing metrics can complement source code metrics by providing an overview of
how well the code is tested, and thus showing information on the dynamic 
(executable) state of the software in addition to the static (source code) view
already supported.

Currently, the Mezuro platform is being developed at the USP FLOSS Competence Center
on the context of NAP-SoL -- Support for Free Software Research,
funded by University of S\~{a}o Paulo.

\bibliographystyle{sbc}
\bibliography{myReferences}
\end{document}
% vim: ts=2 sw=2 expandtab
